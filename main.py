import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras

def print_info(train_images, train_labels, test_images, test_labels):
    print(train_images.shape)
    print(train_labels.shape)
    print(test_images.shape)
    print(test_labels.shape)

def importData():
    mnist = keras.datasets.fashion_mnist
    return mnist.load_data()

def formatData(data):
    return data / 255

def show_plot(train_images):
    plt.figure()
    plt.imshow(train_images[0])
    plt.colorbar()
    plt.grid(False)
    plt.show()

def create_model():
    model = keras.Sequential([
        # Le modèle Sequential est un ensemble linéaire de couches
        keras.layers.Flatten(input_shape=(28,28)),
        # Transforme une matrice 28x28 en un tableau de 784
        keras.layers.Dense(128, activation=tf.nn.relu),
        # Couche entièrement connectée de 128 neurones
        keras.layers.Dense(10, activation=tf.nn.softmax)
        # Couche entièrement connectée de 10 neurones:
        # 10 probabilités de sortie
    ])
    return model

def create_convoluted_model(nb_filters, kernel_size, pool_size):
    model = keras.Sequential([
        # Le modèle Sequential est un ensemble linéaire de couches
        keras.layers.Conv2D(filters=nb_filters, kernel_size=kernel_size, activation=tf.nn.relu, input_shape=(28, 28, 1)),
        keras.layers.MaxPooling2D(pool_size=pool_size),
        keras.layers.Dropout(0.25),
        keras.layers.Flatten(input_shape=(28,28)),
        # Transforme une matrice 28x28 en un tableau de 784
        keras.layers.Dense(128, activation=tf.nn.relu),
        # Couche entièrement connectée de 128 neurones
        keras.layers.Dense(10, activation=tf.nn.softmax)
        # Couche entièrement connectée de 10 neurones:
        # 10 probabilités de sortie
    ])
    return model

def compile(model):
    model.compile(optimizer='sgd',
    # On choisit la descente de gradient
    # stochastique commme optimisation
    loss='sparse_categorical_crossentropy',
    # Définition de la mesure de perte
    # Ici l'entropie croiée
    metrics=['accuracy']
    # Définition de la mesure de performance
    # que l'on souhaite utiliser. Ici la accuracy
    )

def fit(model):
    model.fit(train_images, train_labels, epochs=40)

def print_score(model):
    train_loss, train_acc = model.evaluate(train_images, train_labels)
    print("\n\n\nTrain - perte: {}, accuracy: {}".format(train_loss, train_acc))
    test_loss, test_acc = model.evaluate(test_images, test_labels)
    print("\nTest - perte: {}, accuracy: {}".format(test_loss, test_acc))

(train_images, train_labels), (test_images, test_labels) = importData()
print_info(train_images, train_labels, test_images, test_labels)
#show_plot(train_images)
train_images = formatData(train_images)
test_images = formatData(test_images)
classifier = create_convoluted_model(32, (3, 3), (2, 2))
compile(classifier)
fit(classifier)
print_score(classifier)